/*
 * Plugin name: STL Writer
 * Description: Write an STL file by the provided model data.
 */

 // Headers
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <memory>
#include <cmath>

// Struct definitions
struct Triangle
{
	unsigned int v[3];
	unsigned int n[3];
};

// Typedefs
typedef std::vector<float> Coord;

// Global variables
std::vector<Coord> vertices;
std::vector<Coord> normals;
std::vector<std::shared_ptr<Triangle>> triangles;

// Functions
// Write the data to a binary STL file
void writeFile(const std::string& inFilePath_crstr)
{
	// Header info by the file path, but only used the file name without the extension
	std::string headerInfo = inFilePath_crstr.substr(inFilePath_crstr.find_last_of('/') + 1);
	headerInfo = headerInfo.substr(0, headerInfo.find('.'));
	char head[80];
	strncpy_s(head, headerInfo.c_str(), sizeof(head) - 1);

	// 0 attribute
	char attribute[2] = "0";

	// Trianlges
	unsigned int nTriLong = triangles.size();

	std::ofstream stlFile;
	stlFile.open(inFilePath_crstr.c_str(), std::ios::out | std::ios::binary);

	// Write header data
	stlFile.write(head, sizeof(head));
	stlFile.write((char*)&nTriLong, 4);

	for (size_t i = 0; i < triangles.size(); ++i)
	{
		std::shared_ptr<Triangle> triangle = triangles[i];

		// The STL has one normal vector by a triangle.
		// The ConvertR3D has normal vectors by vertex.
		// The normal vector by face is computed as the average of the vertex normals of the triangle and normalized
		Coord norm1 = normals[triangle->n[0]];
		Coord norm2 = normals[triangle->n[1]];
		Coord norm3 = normals[triangle->n[2]];
		float faceNormalX = (norm1[0] + norm2[0] + norm3[0]) / 3.0f;
		float faceNormalY = (norm1[1] + norm2[1] + norm3[1]) / 3.0f;
		float faceNormalZ = (norm1[2] + norm2[2] + norm3[2]) / 3.0f;
		float length = std::sqrtf((faceNormalX * faceNormalX) + (faceNormalY * faceNormalY) + (faceNormalZ * faceNormalZ));
		faceNormalX /= length;
		faceNormalY /= length;
		faceNormalZ /= length;

		Coord v1 = vertices[triangle->v[0]];
		Coord v2 = vertices[triangle->v[1]];
		Coord v3 = vertices[triangle->v[2]];

		// Write the data
		stlFile.write((char*)&faceNormalX, 4);
		stlFile.write((char*)&faceNormalY, 4);
		stlFile.write((char*)&faceNormalZ, 4);

		stlFile.write((char*)&v1[0], 4);
		stlFile.write((char*)&v1[1], 4);
		stlFile.write((char*)&v1[2], 4);

		stlFile.write((char*)&v2[0], 4);
		stlFile.write((char*)&v2[1], 4);
		stlFile.write((char*)&v2[2], 4);

		stlFile.write((char*)&v3[0], 4);
		stlFile.write((char*)&v3[1], 4);
		stlFile.write((char*)&v3[2], 4);

		stlFile.write(attribute, 2);
	}
	stlFile.close();
}

extern "C"
{
	// Send the information that this plugin can handle only the STL files
	__declspec(dllexport) std::string getExtension()
	{
		return "*.stl";
	}

	// Send the file path information to the file writer function
	__declspec(dllexport) void save(const std::string& inFilePath_crstr)
	{
		writeFile(inFilePath_crstr);
	}

	// Clear all data if a new model given
	__declspec(dllexport) void clear()
	{
		vertices.clear();
		normals.clear();
		triangles.clear();
	}

	// Get the information if the plugin supports the triangles
	__declspec(dllexport) bool isTriangleSupported()
	{
		return true;
	}

	// Get the information if the plugin supports the normal vectors
	__declspec(dllexport) bool isNormalSupported()
	{
		return true;
	}

	// Get the information if the plugin supports the UVs
	__declspec(dllexport) bool isUVSupported()
	{
		return false;
	}

	// Start a new triangle
	__declspec(dllexport) void setNewTriangle()
	{
		std::shared_ptr<Triangle> triangle(new Triangle);
		triangles.push_back(triangle);
	}

	// Add vertex indices to the current trangle
	__declspec(dllexport) void addVertexIndexesToTriangle(unsigned int inV1, unsigned int inV2, unsigned int inV3)
	{
		std::shared_ptr<Triangle> triangle = triangles.back();
		triangle->v[0] = inV1;
		triangle->v[1] = inV2;
		triangle->v[2] = inV3;
	}

	// Add normal indices to the current trangle
	__declspec(dllexport) void addNormalIndexesToTriangle(unsigned int inN1, unsigned int inN2, unsigned int inN3)
	{
		std::shared_ptr<Triangle> triangle = triangles.back();
		triangle->n[0] = inN1;
		triangle->n[1] = inN2;
		triangle->n[2] = inN3;
	}

	// UVs aren't supported by STL
	__declspec(dllexport) void addUVIndexesToTriangle(unsigned int inUV1, unsigned int inUV2, unsigned int inUV3)
	{
	}

	// Add new vertex to the model
	__declspec(dllexport) void addNewVertex(float inX, float inY, float inZ, float inW)
	{
		Coord vertex = { inX, inY, inZ };
		vertices.push_back(vertex);
	}

	// Add new normal to the model
	__declspec(dllexport) void addNewNormal(float inX, float inY, float inZ)
	{
		Coord normal = { inX, inY, inZ };
		normals.push_back(normal);
	}

	// UVs aren't supported by STL
	__declspec(dllexport) void addNewUV(float inU, float inV, float inW)
	{
	}
}
