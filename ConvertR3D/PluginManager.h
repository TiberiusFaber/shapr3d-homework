/*
 * Class name:  PluginManager
 * Description: Parent class for all plugin manager classes. It provides general functions.
 *              Loads all the DLL-s in the specific folder. All the dll-s have to have getExtension() function.
 */

#pragma once

// Qt headers
#include <QStringList>

// STL headers
#include <memory>
#include <map>
#include <string>

// WinAPI headers
#include <windows.h>

// Predeclaration
class ModelManager;

class PluginManager
{
public:
	// Constructor, needs to have the ModelManager object
	PluginManager(std::shared_ptr<ModelManager> inModelManager);
	// Pure virtual destructor
	virtual ~PluginManager() = 0;
	// Main interface function, the derived class implements the way to collect all dll plugins belongs to it
	virtual void registerDllList() = 0;
	// Get the list of the supported file extensions to filter the file opener/saver dialog, all extensions are belonging to a proper plugin dll
	QString getSupportedExtensions();

protected:
	// Called by the drived classes, it collects all dll file paths from the given folder
	void getAllDLL(const std::string& inFolder);

private:
	// Loads the dll files from from the path list and get the supported extension
	void loadAllDLL();
	// Release all the loaded libraries
	void removeAllDLL();

protected:
	std::shared_ptr<ModelManager> _modelMgr;            // Model class
	QStringList                   _supportedExtensions; // List of the supported extensions  
	std::map<QString, HINSTANCE>  _registeredDllList;   // All loaded plugin libraries
	QStringList                   _dllList;             // All dll paths
};

