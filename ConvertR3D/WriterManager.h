/*
 * Class name:  WriterManager
 * Description: Collect and handle all writer plugins in the writer folder.
 */

#pragma once

 // Project headers
#include "PluginManager.h"

// Predeclaration
class ModelManager;

class WriterManager : public PluginManager
{
public:
	WriterManager(std::shared_ptr<ModelManager> inModelManager);
	~WriterManager();

	// Main interface function, handle every plugin dlls inside the writer folder
	void registerDllList();
	// Check if the current model's features and the saver are compatible
	std::string checkCompatibility(const std::string& inFileName_cstr);
	// Save a model to the given file path
	bool saveModel(const std::string& inFileName_cstr);

private:
	// Save the file by the proper plugin
	bool callSaver(const HINSTANCE inHandler_h, const std::string& inFileName_cstr);
};

