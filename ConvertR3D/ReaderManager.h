/*
 * Class name:  ReaderManager
 * Description: Collect and handle all reader plugins in the reader folder.
 */

#pragma once

// Project headers
#include "PluginManager.h"

// STL headers
#include <string>

// Predeclaration
class ModelManager;

class ReaderManager : public PluginManager
{
public:
	// Constructor with the model manager
	ReaderManager(std::shared_ptr<ModelManager> inModelManager);
	~ReaderManager();

	// Main interface function, handle every plugin dlls inside the reader folder
	void registerDllList();
	// Read a model by the given file path
	bool readModel(const std::string& inFileName_cstr);

private:
	// Read the file by the proper plugin
	bool callReader(const HINSTANCE inHandler_h, const std::string& inFileName_cstr);
};

