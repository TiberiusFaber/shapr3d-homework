#include "ReaderManager.h"
#include "ModelManager.h"

typedef int unsigned (CALLBACK* evaluateFileReturnTrianglesType)(const std::string&);
typedef void  (CALLBACK* closeFileType)();
typedef bool  (CALLBACK* hasTriangleType)();
typedef bool  (CALLBACK* hasNormalType)();
typedef bool  (CALLBACK* hasUVType)();
typedef void  (CALLBACK* getNewTriangleType)();
typedef void  (CALLBACK* getNewVertexType)();
typedef float (CALLBACK* getNewVertexXType)();
typedef float (CALLBACK* getNewVertexYType)();
typedef float (CALLBACK* getNewVertexZType)();
typedef float (CALLBACK* getNewVertexWType)();
typedef float (CALLBACK* getNewNormalXType)();
typedef float (CALLBACK* getNewNormalYType)();
typedef float (CALLBACK* getNewNormalZType)();
typedef float (CALLBACK* getNewUVUType)();
typedef float (CALLBACK* getNewUVVType)();
typedef float (CALLBACK* getNewUVWType)();

ReaderManager::ReaderManager(std::shared_ptr<ModelManager> inModelManager)
	: PluginManager(inModelManager)
{
}

ReaderManager::~ReaderManager()
{
}

// Call the geAllDll() function from the parent class to collect every dlls from the readers folder
void ReaderManager::registerDllList()
{
	getAllDLL("/readers");
}

// Get the extension of the file and according to it get the corresponding library and call the callReader() function
bool ReaderManager::readModel(const std::string& inFileName_cstr)
{
	std::string extention = '*' + inFileName_cstr.substr(inFileName_cstr.find_last_of("."));
	HINSTANCE lib = _registeredDllList[QString::fromStdString(extention)];
	if (NULL != lib)
		return callReader(lib, inFileName_cstr);
	return false;
}

// Register all the functions of the selected plugin and read the model
bool ReaderManager::callReader(const HINSTANCE inHandler_h, const std::string& inFileName_cstr)
{
	evaluateFileReturnTrianglesType readerEvaluateFileReturnUnits = (evaluateFileReturnTrianglesType) GetProcAddress(inHandler_h, "evaluateFileReturnUnits");
	closeFileType                   readerCloseFile               = (closeFileType)                   GetProcAddress(inHandler_h, "closeFile");
	hasTriangleType                 readerHasTriangle             = (hasTriangleType)                 GetProcAddress(inHandler_h, "hasTriangle");
	hasNormalType                   readerHasNormal               = (hasNormalType)                   GetProcAddress(inHandler_h, "hasNormal");
	hasUVType                       readerHasUV                   = (hasUVType)                       GetProcAddress(inHandler_h, "hasUV");
	getNewTriangleType              readerGetNewTriangle          = (getNewTriangleType)              GetProcAddress(inHandler_h, "getNewTriangle");
	getNewVertexType                readerGetNewVertex            = (getNewVertexType)                GetProcAddress(inHandler_h, "getNewVertex");
	getNewVertexXType               readerGetNewVertexX           = (getNewVertexXType)               GetProcAddress(inHandler_h, "getNewVertexX");
	getNewVertexYType               readerGetNewVertexY           = (getNewVertexYType)               GetProcAddress(inHandler_h, "getNewVertexY");
	getNewVertexZType               readerGetNewVertexZ           = (getNewVertexZType)               GetProcAddress(inHandler_h, "getNewVertexZ");
	getNewVertexWType               readerGetNewVertexW           = (getNewVertexWType)               GetProcAddress(inHandler_h, "getNewVertexW");
	getNewNormalXType               readerGetNewNormalX           = (getNewNormalXType)               GetProcAddress(inHandler_h, "getNewNormalX");
	getNewNormalYType               readerGetNewNormalY           = (getNewNormalYType)               GetProcAddress(inHandler_h, "getNewNormalY");
	getNewNormalZType               readerGetNewNormalZ           = (getNewNormalZType)               GetProcAddress(inHandler_h, "getNewNormalZ");
	getNewUVUType                   readerGetNewUVU               = (getNewUVUType)                   GetProcAddress(inHandler_h, "getNewUVU");
	getNewUVVType                   readerGetNewUVV               = (getNewUVVType)                   GetProcAddress(inHandler_h, "getNewUVV");
	getNewUVWType                   readerGetNewUVW               = (getNewUVWType)                   GetProcAddress(inHandler_h, "getNewUVW");

	// Check if the functions are valid
	if (   NULL == readerEvaluateFileReturnUnits
		|| NULL == readerCloseFile
		|| NULL == readerHasTriangle
		|| NULL == readerHasNormal
		|| NULL == readerHasUV
		|| NULL == readerGetNewTriangle
		|| NULL == readerGetNewVertex
		|| NULL == readerGetNewVertexX
		|| NULL == readerGetNewVertexY
		|| NULL == readerGetNewVertexW
		|| NULL == readerGetNewVertexW
		|| NULL == readerGetNewNormalX
		|| NULL == readerGetNewNormalY
		|| NULL == readerGetNewNormalZ
		|| NULL == readerGetNewUVU
		|| NULL == readerGetNewUVV
		|| NULL == readerGetNewUVW)
	{
		return false;
	}

	// Get the unit count. It can be triangles or vertices according to if its a model or a pointcloud
	const unsigned int units = readerEvaluateFileReturnUnits(inFileName_cstr);
	bool isHasTriangles = readerHasTriangle();

	// Start the model creation
	_modelMgr.get()->createModel();

	// If a model is a polygon
	if (isHasTriangles)
	{
		// Walk through all triangles
		for (unsigned int i = 0; i < units; ++i)
		{
			// Get all triangles and all point properties if exists
			readerGetNewTriangle();
			_modelMgr.get()->startTriangle();
			for (size_t i = 0; i < 3; ++i)
			{
				readerGetNewVertex();
				_modelMgr.get()->addVertex(readerGetNewVertexX(),
										   readerGetNewVertexY(),
										   readerGetNewVertexZ(),
										   readerGetNewVertexW());
				if (readerHasNormal())
				{
					_modelMgr.get()->addNormal(readerGetNewNormalX(),
											   readerGetNewNormalY(),
											   readerGetNewNormalZ());
				}
				if (readerHasUV())
				{
					_modelMgr.get()->addNormal(readerGetNewUVU(),
											   readerGetNewUVV(),
											   readerGetNewUVW());
				}
			}
			_modelMgr.get()->endTriangle();
		}
	}
	// If the model is a pointcloud
	else
	{
		// Walk through all vertices and get the other information
		for (unsigned int i = 0; i < units; ++i)
		{
			readerGetNewVertex();
			_modelMgr.get()->addVertex(readerGetNewVertexX(),
									   readerGetNewVertexY(),
									   readerGetNewVertexZ(),
									   readerGetNewVertexW());
			if (readerHasNormal())
			{
				_modelMgr.get()->addNormal(readerGetNewNormalX(),
										   readerGetNewNormalY(),
										   readerGetNewNormalZ());
			}
			if (readerHasUV())
			{
				_modelMgr.get()->addNormal(readerGetNewUVU(),
										   readerGetNewUVV(),
										   readerGetNewUVW());
			}
		}
	}

	readerCloseFile();

	return true;
}
