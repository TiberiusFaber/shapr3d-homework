// Include header
#include "ModelManager.h"

// STL includes
#include <sstream>
#include <iomanip>


ModelManager::ModelManager()
	: _isTriangle(false)
	, _vertexCnt(0)
	, _normalCnt(0)
	, _uvCnt(0)
	, _unoptimizeHelper(0.0f)
	, _isUsingTriangle(false)
	, _isUsingNormal(false)
	, _isUsingUV(false)
	, _currentTriangleIdx(0)
{
}


ModelManager::~ModelManager()
{
}

// Calls the clearModel() function to clear every data.
void ModelManager::createModel()
{
	clearModel();
}

// Every triangle should be added with starting this function. It sets the flag that it is a triangle and
// sets true the using triangle flag.
void ModelManager::startTriangle()
{
	_isTriangle = true;
	_isUsingTriangle = true;
}

void ModelManager::endTriangle()
{
	_isTriangle = false;
	_vertexCnt = 0;
	_normalCnt = 0;
	_uvCnt = 0;
	_triangles.push_back(_currentTriangle);
}

// The new vertex will be added to the vertices map. If the vertex is uploaded within a triangle the repeating vertices won't be stored
void ModelManager::addVertex(const float inX_cf32, const float inY_cf32, const float inZ_cf32, const float inW_cf32)
{
	Coord coord = { inX_cf32, inY_cf32, inZ_cf32, inW_cf32 };
	FtrPtr vPtr = newVertex(coord, _isTriangle);
	if (_isTriangle)
	{
		if (_vertexCnt > 2)
		{
			throw std::runtime_error("Triangle got more then 3 vertices!");
		}

		_currentTriangle.v[_vertexCnt] = vPtr;
		++_vertexCnt;
	}
}

// The new normal will be added to the normals map. If the normal is uploaded within a triangle the repeating normals won't be stored
void ModelManager::addNormal(const float inX_cf32, const float inY_cf32, const float inZ_cf32)
{
	Coord coord = { inX_cf32, inY_cf32, inZ_cf32 };
	FtrPtr nPtr = newNormal(coord, _isTriangle);
	if (_isTriangle)
	{
		if (_normalCnt > 2)
		{
			throw std::runtime_error("Triangle got more then 3 normals!");
		}

		_currentTriangle.n[_normalCnt] = nPtr;
		++_normalCnt;
	}
	_isUsingNormal = true;
}

// The new UV will be added to the UVs map. If the UV is uploaded within a triangle the repeating UVs won't be stored
void ModelManager::addUV(const float inU_cf32, const float inV_cf32, const float inW_cf32)
{
	Coord coord = { inU_cf32, inV_cf32, inW_cf32 };
	FtrPtr tPtr = newNormal(coord, _isTriangle);
	if (_isTriangle)
	{
		if (_uvCnt > 2)
		{
			throw std::runtime_error("Triangle got more then 3 UVs!");
		}

		_currentTriangle.t[_uvCnt] = tPtr;
		++_uvCnt;
	}

	_isUsingUV = true;
}

// It sets the current triangle index to the first in the vector
void ModelManager::startReadTriangles()
{
	_currentTriangleIdx = 0;
}

// Set the current triangle and increase the index
void ModelManager::readTriangle()
{
	if (_currentTriangleIdx >= _triangles.size())
		return;

	_currentTriangle = _triangles[_currentTriangleIdx];
	++_currentTriangleIdx;
}

// Read one of the tree vertex indices of the current triangle
unsigned int ModelManager::getVertexIndexOfCurrentTriangle(const unsigned int inPoint_cui)
{
	return _currentTriangle.v[inPoint_cui]->first;
}

// Read one of the tree normal indices of the current triangle
unsigned int ModelManager::getNormalIndexOfCurrentTriangle(const unsigned int inPoint_cui)
{
	if (!_isUsingNormal)
		return 0;
	return _currentTriangle.n[inPoint_cui]->first;
}

// Read one of the tree UV indices of the current triangle
unsigned int ModelManager::getUVIndexOfCurrentTriangle(const unsigned int inPoint_cui)
{
	if (!_isUsingUV)
		return 0;
	return _currentTriangle.t[inPoint_cui]->first;
}

// Creates a vector with the size of the vertices and write the proper vertex to the proper index
std::vector<Coord> ModelManager::getVertices()
{
	std::vector<Coord> vertices;
	vertices.resize(_vertices.size());
	for (auto it = _vertices.begin(); it != _vertices.end(); ++it)
	{
		vertices[it->second->first] = it->second->second;
	}
	return vertices;
}

// Creates a vector with the size of the normals and write the proper normal to the proper index
std::vector<Coord> ModelManager::getNormals()
{
	std::vector<Coord> normals;
	normals.resize(_normals.size());
	for (auto it = _normals.begin(); it != _normals.end(); ++it)
	{
		normals[it->second->first] = it->second->second;
	}
	return normals;
}

// Creates a UV with the size of the UVs and write the proper UV to the proper index
std::vector<Coord> ModelManager::getUVs()
{
	std::vector<Coord> uvs;
	uvs.resize(_uvs.size());
	for (auto it = _uvs.begin(); it != _uvs.end(); ++it)
	{
		uvs[it->second->first] = it->second->second;
	}
	return uvs;
}

// Clear the model data and reset everything to 0
void ModelManager::clearModel()
{
	_triangles.clear();
	_vertices.clear();
	_normals.clear();
	_uvs.clear();
	_vertexCnt = 0;
	_normalCnt = 0;
	_uvCnt = 0;
	_unoptimizeHelper = 0.0f;
	_isUsingTriangle = false;
	_isUsingNormal = false;
	_isUsingUV = false;
}

// Create a key by the input coordinate. It creates a string from the float coordinates with precision 6
std::string ModelManager::coordToStr(const std::vector<float>& inCoord_cvf32)
{
	std::stringstream ss;
	ss << std::fixed << std::setprecision(6);
	for (const float& i : inCoord_cvf32)
		ss << i << ";";
	return ss.str();
}

// Add a new vertex to the vertices map. A unique index will be added to that map.
// If the optimize flag is true, the repeating vertices will be avoided.
// The new vertex's pointer will be returned.
// If the new vertex is avoided, the existing vertex pointer will be returned.
FtrPtr ModelManager::newVertex(const Coord& inCoord_cvf32, bool inOptimize_b)
{
	std::string key;
	if (inOptimize_b)
	{
		key = coordToStr(inCoord_cvf32);
		auto found = _vertices.find(key);
		if (found == _vertices.end())
		{
			size_t newIdx = _vertices.size();
			_vertices[key] = std::make_shared<Ftr>(std::make_pair(newIdx, inCoord_cvf32));
		}
		else
		{
			key = found->first;
		}
	}
	else
	{
		Coord keyFloat = { _unoptimizeHelper };
		key = coordToStr(keyFloat);
		size_t newIdx = _vertices.size();
		_vertices[key] = std::make_shared<Ftr>(std::make_pair(newIdx, inCoord_cvf32));
		
		_unoptimizeHelper += 1.0f;
	}
	return _vertices[key];
}

// Add a new normal to the normals map. A unique index will be added to that map.
// If the optimize flag is true, the repeating normals will be avoided.
// The new normal's pointer will be returned.
// If the new normal is avoided, the existing normal pointer will be returned.
FtrPtr ModelManager::newNormal(const Coord& inCoord_cvf32, bool inOptimize_b)
{
	std::string key;
	if (inOptimize_b)
	{
		key = coordToStr(inCoord_cvf32);
		auto found = _normals.find(key);
		if (found == _normals.end())
		{
			size_t newIdx = _normals.size();
			_normals[key] = std::make_shared<Ftr>(std::make_pair(newIdx, inCoord_cvf32));
		}
		else
		{
			key = found->first;
		}
	}
	else
	{
		Coord keyFloat = { _unoptimizeHelper };
		key = coordToStr(keyFloat);
		size_t newIdx = _normals.size();
		_normals[key] = std::make_shared<Ftr>(std::make_pair(newIdx, inCoord_cvf32));

		_unoptimizeHelper += 1.0f;
	}
	return _normals[key];
}

// Add a new UV to the UVs map. A unique index will be added to that map.
// If the optimize flag is true, the repeating UVs will be avoided.
// The new UV's pointer will be returned.
// If the new UV is avoided, the existing UV pointer will be returned.
FtrPtr ModelManager::newUV(const Coord& inCoord_cvf32, bool inOptimize_b)
{
	std::string key;
	if (inOptimize_b)
	{
		key = coordToStr(inCoord_cvf32);
		auto found = _uvs.find(key);
		if (found == _uvs.end())
		{
			size_t newIdx = _uvs.size();
			_uvs[key] = std::make_shared<Ftr>(std::make_pair(newIdx, inCoord_cvf32));
		}
		else
		{
			key = found->first;
		}
	}
	else
	{
		Coord keyFloat = { _unoptimizeHelper };
		key = coordToStr(keyFloat);
		size_t newIdx = _uvs.size();
		_uvs[key] = std::make_shared<Ftr>(std::make_pair(newIdx, inCoord_cvf32));

		_unoptimizeHelper += 1.0f;
	}
	return _uvs[key];
}
