/*
 * Class name:  ModelManager
 * Description: Stores the opened 3D model data. If the 3D model is triangle based,
 *              every vertices, normals and uvs will be stored only once.
 */

#pragma once

// Headers
#include <memory>
#include <unordered_map>
#include <vector>

// Type definitions
typedef std::pair<unsigned int, std::vector<float>> Ftr;
typedef std::shared_ptr<Ftr> FtrPtr;
typedef std::vector<float> Coord;

// Structure definitions
struct Triangle
{
	FtrPtr v[3];
	FtrPtr n[3];
	FtrPtr t[3];
};

class ModelManager
{
public:
	// Constructor
	ModelManager();
	// Destructor
	~ModelManager();

	// Clear the storage to start the uploding with clean sheet
	void createModel();
	// If the model triangle based, the vertex- normal- and uv data should be added after to call this function
	void startTriangle();
	// After all of the vertices, normals and uvs are added to a triangle, this function have to be called
	void endTriangle();
	// Add vertex point X, Y, Z and W
	void addVertex(const float inX_cf32, const float inY_cf32, const float inZ_cf32, const float inW_cf32);
	// Add normal vector X, Y and Z
	void addNormal(const float inX_cf32, const float inY_cf32, const float inZ_cf32);
	// Add UV coordinates U, V and W
	void addUV(const float inU_cf32, const float inV_cf32, const float inW_cf32);

	// Get the feature information
	// Does the model based on triangles or only a point cloud?
	bool getIsUsingTriangle() const { return _isUsingTriangle; }
	// Does the model contains normals?
	bool getIsUsingNormal()   const { return _isUsingNormal; }
	// Does the model contains UVs?
	bool getIsUsingUV()       const { return _isUsingUV; }

	// Get the count of the triangles in the model
	size_t getTrianglesCnt() const { return _triangles.size(); }

	// Before write the model call this function
	void startReadTriangles();
	// Every triangle read have to this function, it sets the current triangle to read
	void readTriangle();
	// It reads one vertex index from the current triangle, the input can be 0, 1 or 2
	unsigned int getVertexIndexOfCurrentTriangle(const unsigned int inPoint_cui);
	// It reads one normal index from the current triangle, the input can be 0, 1 or 2
	unsigned int getNormalIndexOfCurrentTriangle(const unsigned int inPoint_cui);
	// It reads one UV index from the current triangle, the input can be 0, 1 or 2
	unsigned int getUVIndexOfCurrentTriangle(const unsigned int inPoint_cui);

	// Returns all vertices
	std::vector<Coord> getVertices();
	// Returns all normals
	std::vector<Coord> getNormals();
	// Returns all UVs
	std::vector<Coord> getUVs();

private:
	// Clear the modell from all data
	void clearModel();
	// Generates a key from a coordinate
	std::string coordToStr(const std::vector<float>& inCoord_cvf32);

	// Adds a new vertex to the vertex map, but if the optimize flag is true it adds only the unique vertices and avoids the repeated ones.
	FtrPtr newVertex(const Coord& inCoord_cvf32, bool inOptimize_b);
	// Adds a new normal to the normal map, but if the optimize flag is true it adds only the unique normals and avoids the repeated ones.
	FtrPtr newNormal(const Coord& inCoord_cvf32, bool inOptimize_b);
	// Adds a new UV to the UV map, but if the optimize flag is true it adds only the unique UVs and avoids the repeated ones.
	FtrPtr newUV(const Coord& inCoord_cvf32, bool inOptimize_b);

private:
	bool         _isTriangle; // If the upload is a triangle, the vertex/normal/UV uploads to a triangle
	unsigned int _vertexCnt;  // Holds the index of a vertex of a triangle at the uploading
	unsigned int _normalCnt;  // Holds the index of a normal of a triangle at the uploading
	unsigned int _uvCnt;      // Holds the index of a UV of a triangle at the uploading
	float        _unoptimizeHelper; // If only pointcloud is uploaded, each vertex/normal/UV will be stored and all of them will have unique key according to this helper
	bool         _isUsingTriangle; // Information if the modell has triangles 
	bool         _isUsingNormal;   // Information if the modell has normals
	bool         _isUsingUV;       // Information if the modell has UVs
	size_t _currentTriangleIdx; // At read triangles this variable shows which triangle is readed
	std::unordered_map<std::string, FtrPtr> _vertices; // All vertices
	std::unordered_map<std::string, FtrPtr> _normals;  // All normals
	std::unordered_map<std::string, FtrPtr> _uvs;      // All UVs
	std::vector<Triangle> _triangles;                  // All triangles
	Triangle _currentTriangle; // Current triangle at read and at upload

};

