#include "WriterManager.h"
#include "ModelManager.h"

typedef void (CALLBACK* saveType)(const std::string&);
typedef void (CALLBACK* clearType)();
typedef bool (CALLBACK* isTriangleSupportedType)();
typedef bool (CALLBACK* isNormalSupportedType)();
typedef bool (CALLBACK* isUVSupportedType)();
typedef void (CALLBACK* setNewTriangleType)();
typedef void (CALLBACK* addVertexIndexesToTriangleType)(unsigned int, unsigned int, unsigned int);
typedef void (CALLBACK* addNormalIndexesToTriangleType)(unsigned int, unsigned int, unsigned int);
typedef void (CALLBACK* addUVIndexesToTriangleType)(unsigned int, unsigned int, unsigned int);
typedef void (CALLBACK* addNewVertexType)(float, float, float, float);
typedef void (CALLBACK* addNewNormalType)(float, float, float);
typedef void (CALLBACK* addNewUVType)(float, float, float);

WriterManager::WriterManager(std::shared_ptr<ModelManager> inModelManager)
	: PluginManager(inModelManager)
{
}

WriterManager::~WriterManager()
{
}

// Call the geAllDll() function from the parent class to collect every dlls from the writers folder
void WriterManager::registerDllList()
{
	getAllDLL("/writers");
}

// If the current model has a feature but the saver can't save it generate a warning message
std::string WriterManager::checkCompatibility(const std::string& inFileName_cstr)
{
	std::string extention = '*' + inFileName_cstr.substr(inFileName_cstr.find_last_of("."));
	HINSTANCE lib = _registeredDllList[QString::fromStdString(extention)];
	if (NULL == lib) return "Problem with the DLL!";

	isTriangleSupportedType writerIsTriangleSupported = (isTriangleSupportedType)GetProcAddress(lib, "isTriangleSupported");
	isNormalSupportedType   writerIsNormalSupported   = (isNormalSupportedType)  GetProcAddress(lib, "isNormalSupported");
	isUVSupportedType       writerIsUVSupported       = (isUVSupportedType)      GetProcAddress(lib, "isUVSupported");

	if (   NULL == writerIsTriangleSupported
		|| NULL == writerIsNormalSupported
		|| NULL == writerIsUVSupported)
	{
		return "Problem with the DLL!";
	}

	std::string res = "";
	if (_modelMgr->getIsUsingTriangle() != writerIsTriangleSupported())
		res += "The two file format supports the triangle differently!\n";
	if (_modelMgr->getIsUsingNormal() != writerIsNormalSupported())
		res += "The two file format supports the normals differently!\n";
	if (_modelMgr->getIsUsingUV() != writerIsUVSupported())
		res += "The two file format supports the UVs differently!\n";

	if (!res.empty())
		res += "Would you like to continue?";

	return res;
}

// Get the extension of the file and according to it get the corresponding library and call the callSaver() function
bool WriterManager::saveModel(const std::string& inFileName_cstr)
{
	std::string extention = '*' + inFileName_cstr.substr(inFileName_cstr.find_last_of("."));
	HINSTANCE lib = _registeredDllList[QString::fromStdString(extention)];
	if (NULL != lib)
		return callSaver(lib, inFileName_cstr);
	return false;
}

// Register all the functions of the selected plugin and save the model
bool WriterManager::callSaver(const HINSTANCE inHandler_h, const std::string& inFileName_cstr)
{
	saveType                       writerSave                       = (saveType)                      GetProcAddress(inHandler_h, "save");
	clearType                      writerClear                      = (clearType)                     GetProcAddress(inHandler_h, "clear");
	setNewTriangleType             writerSetNewTriangle             = (setNewTriangleType)            GetProcAddress(inHandler_h, "setNewTriangle");
	addVertexIndexesToTriangleType writerAddVertexIndexesToTriangle = (addVertexIndexesToTriangleType)GetProcAddress(inHandler_h, "addVertexIndexesToTriangle");
	addNormalIndexesToTriangleType writerAddNormalIndexesToTriangle = (addNormalIndexesToTriangleType)GetProcAddress(inHandler_h, "addNormalIndexesToTriangle");
	addUVIndexesToTriangleType     writerAddUVIndexesToTriangle     = (addUVIndexesToTriangleType)    GetProcAddress(inHandler_h, "addUVIndexesToTriangle");
	addNewVertexType               writerAddNewVertex               = (addNewVertexType)              GetProcAddress(inHandler_h, "addNewVertex");
	addNewNormalType               writerAddNewNormal               = (addNewNormalType)              GetProcAddress(inHandler_h, "addNewNormal");
	addNewUVType                   writerAddNewUV                   = (addNewUVType)                  GetProcAddress(inHandler_h, "addNewUV");
	isTriangleSupportedType        writerIsTriangleSupported        = (isTriangleSupportedType)       GetProcAddress(inHandler_h, "isTriangleSupported");
	isNormalSupportedType          writerIsNormalSupported          = (isNormalSupportedType)         GetProcAddress(inHandler_h, "isNormalSupported");
	isUVSupportedType              writerIsUVSupported              = (isUVSupportedType)             GetProcAddress(inHandler_h, "isUVSupported");

	// Check if the functions are valid
	if (   NULL == writerSave
		|| NULL == writerClear
		|| NULL == writerSetNewTriangle
		|| NULL == writerAddVertexIndexesToTriangle
		|| NULL == writerAddNormalIndexesToTriangle
		|| NULL == writerAddUVIndexesToTriangle
		|| NULL == writerAddNewVertex
		|| NULL == writerAddNewNormal
		|| NULL == writerAddNewUV
		|| NULL == writerIsTriangleSupported
		|| NULL == writerIsNormalSupported
		|| NULL == writerIsUVSupported)
	{
		return false;
	}

	// Clear the writer to clear every disturbing data
	writerClear();

	// If triangle supported send the point indices
	if (writerIsTriangleSupported())
	{
		_modelMgr->startReadTriangles();
		unsigned int triangleCnt = _modelMgr->getTrianglesCnt();
		// Walk through all triangles
		for (unsigned int i = 0; i < triangleCnt; ++i)
		{
			_modelMgr->readTriangle();
			writerSetNewTriangle();
			writerAddVertexIndexesToTriangle(_modelMgr->getVertexIndexOfCurrentTriangle(0),
											 _modelMgr->getVertexIndexOfCurrentTriangle(1),
											 _modelMgr->getVertexIndexOfCurrentTriangle(2));
			if (writerIsNormalSupported())
			{
				writerAddNormalIndexesToTriangle(_modelMgr->getNormalIndexOfCurrentTriangle(0),
												 _modelMgr->getNormalIndexOfCurrentTriangle(1),
												 _modelMgr->getNormalIndexOfCurrentTriangle(2));
			}
			if (writerIsUVSupported())
			{
				writerAddUVIndexesToTriangle(_modelMgr->getUVIndexOfCurrentTriangle(0),
											 _modelMgr->getUVIndexOfCurrentTriangle(1),
											 _modelMgr->getUVIndexOfCurrentTriangle(2));
			}
		}
	}

	// Write all vertices, normals and uvs
	std::vector<Coord> vertices = _modelMgr->getVertices();
	std::vector<Coord> normals  = _modelMgr->getNormals();
	std::vector<Coord> uvs      = _modelMgr->getUVs();

	for (size_t i = 0; i < vertices.size(); ++i)
	{
		writerAddNewVertex(vertices[i][0], vertices[i][1], vertices[i][2], vertices[i][3]);
	}

	if (writerIsNormalSupported())
	{
		for (size_t i = 0; i < normals.size(); ++i)
		{
			writerAddNewNormal(normals[i][0], normals[i][1], normals[i][2]);
		}
	}

	if (writerIsUVSupported())
	{
		for (size_t i = 0; i < uvs.size(); ++i)
		{
			writerAddNewUV(uvs[i][0], uvs[i][1], uvs[i][2]);
		}
	}

	// Close the sending and write the file
	writerSave(inFileName_cstr);

	return true;
}
