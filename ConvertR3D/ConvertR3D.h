/*
 * Class name:  ConvertR3D
 * Description: Creates the main window, handles the user events. Starts the
 *              "open file" and the "save file" event.
 */

#pragma once

// Headers
#include <QtWidgets/QMainWindow>
#include <memory>
#include "ui_ConvertR3D.h"

// Predeclaration
class ReaderManager;
class WriterManager;
class ModelManager;

class ConvertR3D : public QMainWindow
{
	Q_OBJECT

public:
	// Constructor
	ConvertR3D(QWidget* parent = Q_NULLPTR);

	// Destructor
	~ConvertR3D();

private:
	// Init the software, calls the reader- and the writer manager to mount the plugins.
	void init();

private slots:
	// Open the supported 3D files
	void openFileSlot();

	// Save the opened file on another supported 3D file
	void saveFileSlot();

private:
	std::shared_ptr<ModelManager>  _modelMgr;
	std::unique_ptr<ReaderManager> _readerMgr;
	std::unique_ptr<WriterManager> _writerMgr;
	Ui::ConvertR3DClass _ui;
};
