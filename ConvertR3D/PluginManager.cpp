#include "PluginManager.h"
#include "ModelManager.h"

#include <QDir>

typedef std::string(CALLBACK* GetExtensionType)();

PluginManager::PluginManager(std::shared_ptr<ModelManager> inModelManager)
	: _modelMgr(inModelManager)
{
}

// At release remove all dlls
PluginManager::~PluginManager()
{
	removeAllDLL();
}

// Get the supported extension lists and convert it to a string which can be used by the file opener/saver dialog to filter the files
QString PluginManager::getSupportedExtensions()
{
	QStringList exts;
	for (auto it = _registeredDllList.begin(); it != _registeredDllList.end(); ++it)
	{
		exts.push_back(it->first);
	}
	return exts.join(" ");
}

// Collect the path of dlls in a certain folder
void PluginManager::getAllDLL(const std::string& inFolder)
{
	QString dir = QDir::currentPath() + QString(inFolder.c_str());
	QDir directory(dir);
	_dllList = directory.entryList(QStringList() << "*.dll" << "*.DLL", QDir::Files);
	for (size_t i = 0; i < _dllList.size(); ++i)
	{
		_dllList[i] = dir + '/' + _dllList[i];
	}

	loadAllDLL();
}

// Load all dlls according to the dll path list and call the getExtension() function of them
void PluginManager::loadAllDLL()
{
	removeAllDLL();
	foreach(QString str, _dllList)
	{
		HINSTANCE dllHandler = NULL;
		dllHandler = LoadLibrary(str.toStdWString().c_str());
		if (NULL != dllHandler)
		{
			GetExtensionType getExtension = (GetExtensionType)GetProcAddress(dllHandler, "getExtension");
			if (NULL != getExtension)
			{
				_registeredDllList[QString::fromStdString(getExtension())] = dllHandler;
			}
		}
	}
}

// Free all loaded dll libraries
void PluginManager::removeAllDLL()
{
	for (auto it = _registeredDllList.begin(); it != _registeredDllList.end(); ++it)
	{
		if (NULL == it->second)
			continue;

		FreeLibrary(it->second);
	}
	_registeredDllList.clear();
}