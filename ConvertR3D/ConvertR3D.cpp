#include "ConvertR3D.h"

#include "ReaderManager.h"
#include "WriterManager.h"
#include "ModelManager.h"

#include <QFileDialog>
#include <QMessageBox>

ConvertR3D::ConvertR3D(QWidget* parent)
	: QMainWindow(parent)
	, _modelMgr(new ModelManager)
	, _readerMgr(new ReaderManager(_modelMgr))
	, _writerMgr(new WriterManager(_modelMgr))
{
	_ui.setupUi(this);

	// If nothing is opened, the save button should be disabled.
	_ui.savePushButton->setEnabled(false);

	// Connect the buttons to the corresponding slots.
	connect(_ui.openPushButton, &QPushButton::clicked, this, &ConvertR3D::openFileSlot);
	connect(_ui.savePushButton, &QPushButton::clicked, this, &ConvertR3D::saveFileSlot);

	// Call the init function.
	init();
}

ConvertR3D::~ConvertR3D() = default;

// Call reader- and writer manager to mount all plugins.
void ConvertR3D::init()
{
	_readerMgr->registerDllList();
	_writerMgr->registerDllList();
}

// Get the supported extensions, open the file opener dialog with the restriction to known
// extensions and if the file could be opened, enable the save button.
void ConvertR3D::openFileSlot()
{
	QString ext = _readerMgr->getSupportedExtensions();
	QString filename = QFileDialog::getOpenFileName(this, "Open 3D formats", QString(), ext);
	if (filename.isEmpty())
		return;
	if (_readerMgr->readModel(filename.toStdString()))
	{
		_ui.savePushButton->setEnabled(true);
		_ui.label->setText("The model is loaded!");
	}
}

// Get the supported extensions, open the file saver dialog with the restriction to known
// extensions and if the file could be saved, check if the opened and the savable file are
// compatible. If not, ask the user if he/she wants to continue.
void ConvertR3D::saveFileSlot()
{
	QString ext = _writerMgr.get()->getSupportedExtensions();
	QString fileName = QFileDialog::getSaveFileName(this, "Save 3D formats", QString(), ext);
	if (fileName.isEmpty())
		return;
	std::string message = _writerMgr->checkCompatibility(fileName.toStdString());
	if (!message.empty())
	{
		QMessageBox::StandardButton answer = QMessageBox::warning(this, "Warning", QString::fromStdString(message), QMessageBox::Yes | QMessageBox::No);
		if (answer == QMessageBox::No)
			return;
	}
	if (_writerMgr->saveModel(fileName.toStdString()))
	{
		_ui.label->setText("The model is saved!");
	}
}
