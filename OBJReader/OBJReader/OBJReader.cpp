/*
 * Plugin name: OBJ Reader
 * Description: Read the OBJ file and provide its data.
 */

// Headers
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <memory>

// Struct definitions
struct Triangle
{
	unsigned int v[3];
	unsigned int n[3];
	unsigned int t[3];
};

// Typedefs
typedef std::vector<float> Coord;

// Global variables
std::vector<Coord> vertices;
std::vector<Coord> normals;
std::vector<Coord> uvs;
std::vector<std::shared_ptr<Triangle>> triangles;

bool isTriangle = false; // Has the object triangles?
bool isNormal   = false; // Has the object normals?
bool isUV       = false; // Has the object UVs?

unsigned int currentTriangle = 0; // The currently readed triangle index
unsigned int currentVertex   = 0; // The currently readed vertex from a point of the currently readed triangle
unsigned int currentNormal   = 0; // The currently readed normal from a point of the currently readed triangle
unsigned int currentUV       = 0; // The currently readed UV from a point of the currently readed triangle

// Functions
// Split function, splits the input string by the given delimiter
std::vector<std::string> split(const std::string& inString_crstr, char inDelimiter_ch)
{
	std::vector<std::string> tokens;
	std::string token;
	std::istringstream tokenStream(inString_crstr);
	while (std::getline(tokenStream, token, inDelimiter_ch))
	{
		tokens.push_back(token);
	}
	return tokens;
}

// Get the vertex information
void evaluateVertex(const std::string& inLine_crstr)
{
	// Get to know if the line describes an vertex coordinate
	size_t position = inLine_crstr.find("v ");
	if (position == std::string::npos)
		return;

	// Only vertex data
	std::string line = inLine_crstr.substr(2);

	// Explode the string to coordinates
	std::vector<std::string> coords = split(line, ' ');

	// Convert the string data to real values
	Coord newCoordinate;
	size_t i = 0;
	for (const std::string& v : coords)
	{
		newCoordinate.push_back(std::stof(v));
	}

	// By default the W coordinate is 0
	if (coords.size() < 4)
		newCoordinate.push_back(1.0);

	vertices.push_back(newCoordinate);
}

// Get the normal information
void evaluateNormal(const std::string& inLine_crstr)
{
	// Get to know if the line describes an normal vector
	size_t position = inLine_crstr.find("vn ");
	if (position == std::string::npos)
		return;

	// Only normal data
	std::string line = inLine_crstr.substr(3);

	// Explode the string to coordinates
	std::vector<std::string> coords = split(line, ' ');

	// Convert the string data to real values
	Coord newCoordinate;
	size_t i = 0;
	for (const std::string& n : coords)
	{
		newCoordinate.push_back(std::stof(n));
	}

	normals.push_back(newCoordinate);
}

// Get the UV information
void evaluateUVs(const std::string& inLine_crstr)
{
	// Get to know if the line describes an UV point
	size_t position = inLine_crstr.find("vt ");
	if (position == std::string::npos)
		return;

	// Only UV data
	std::string line = inLine_crstr.substr(3);

	// Explode the string to coordinates
	std::vector<std::string> coords = split(line, ' ');

	// Convert the string data to real values
	Coord newCoordinate;
	size_t i = 0;
	for (const std::string& t : coords)
	{
		newCoordinate.push_back(std::stof(t));
	}

	// By default the W coordinate is 0
	if (coords.size() < 3)
		newCoordinate.push_back(0.0);

	uvs.push_back(newCoordinate);
}

// Get the point index information from a face. If a face is a quad split it to two triangles
void evaluateFace(const std::string& inLine_crstr)
{
	// Get to know if the line describes a face
	size_t position = inLine_crstr.find("f ");
	if (position == std::string::npos)
		return;

	isTriangle = true;

	// Only face data
	std::string line = inLine_crstr.substr(2);

	// Explode to point data
	std::vector<std::string> vertexIndices = split(line, ' ');
	// 3 or 4 times {order: vertex, UV, normal}
	std::vector<std::vector<unsigned int>> allIndices;
	for (const std::string& idx : vertexIndices)
	{
		// Explode a point data to vertex-, UV- and normal index
		std::vector<std::string> openedVertexData = split(idx, '/');
		std::vector<unsigned int> indices;

		// Only the vertex data mandatory
		indices.push_back(static_cast<unsigned int>(std::stoi(openedVertexData[0]) - 1)); // It points to array index, therefore decrease

		// If the UV empty, let it be 0
		if (openedVertexData[1].empty())
		{
			indices.push_back(0);
		}
		else
		{
			indices.push_back(static_cast<unsigned int>(std::stoi(openedVertexData[1]) - 1)); // It points to array index, therefore decrease
			isUV = true; // If the UV isn't empty there is UV in the file
		}

		// If the normal empty, let it be 0
		if (openedVertexData[2].empty())
		{
			indices.push_back(0);
		}
		else
		{
			indices.push_back(static_cast<unsigned int>(std::stoi(openedVertexData[2]) - 1)); // It points to array index, therefore decrease
			isNormal = true; // If the normal isn't empty there is normal in the file
		}
		allIndices.push_back(indices);
	}

	// If the face is a triangle, just save it as it is
	if (allIndices.size() == 3)
	{
		std::shared_ptr<Triangle> triangle(new Triangle);
		for (size_t i = 0; i < 3; ++i)
		{
			triangle->v[i] = allIndices[i][0];
			triangle->t[i] = allIndices[i][1];
			triangle->n[i] = allIndices[i][2];
		}
		triangles.push_back(triangle);
	}
	// If the face is a quad, split it
	else
	{
		// Quad: 0, 1, 2, 3 -> Triangle: 0, 1, 2
		{
			std::shared_ptr<Triangle> triangle(new Triangle);
			for (size_t i = 0; i < 3; ++i)
			{
				triangle->v[i] = allIndices[i][0];
				triangle->t[i] = allIndices[i][1];
				triangle->n[i] = allIndices[i][2];
			}
			triangles.push_back(triangle);
		}
		// Quad: 0, 1, 2, 3 -> Triangle: 0, 2, 3
		{
			allIndices.erase(allIndices.begin() + 1);
			std::shared_ptr<Triangle> triangle(new Triangle);
			for (size_t i = 0; i < 3; ++i)
			{
				triangle->v[i] = allIndices[i][0];
				triangle->t[i] = allIndices[i][1];
				triangle->n[i] = allIndices[i][2];
			}
			triangles.push_back(triangle);
		}
	}
}

// Walk through on the OBJ file and evaluate each line
unsigned int evaluateFile(const std::string& inFilePath_crstr)
{
	std::ifstream objFile(inFilePath_crstr.c_str());

	if (!objFile.is_open())
		return 0;

	std::string line;
	while (std::getline(objFile, line))
	{
		evaluateVertex(line);
		evaluateNormal(line);
		evaluateUVs(line);
		evaluateFace(line);
	}

	objFile.close();

	if (isTriangle)
		return triangles.size();
	else
		return vertices.size();
}

// Reset the reader
void clear()
{
	vertices.clear();
	normals.clear();
	uvs.clear();
	isTriangle = false;
	isNormal = false;
	isUV = false;
	currentTriangle = 0;
	currentVertex = 0;
	currentNormal = 0;
	currentUV = 0;
}

extern "C"
{
	// Send the information that this plugin can handle only the OBJ files
	__declspec(dllexport) std::string getExtension()
	{
		return "*.obj";
	}

	// Clear the data inside this plugin, open the file and interpret it. Then return the triangles-, or the vertex count
	__declspec(dllexport) unsigned int evaluateFileReturnUnits(const std::string& inFilePath_crstr)
	{
		clear();
		return evaluateFile(inFilePath_crstr);
	}

	// Has the model triangle?
	__declspec(dllexport) bool hasTriangle()
	{
		return isTriangle;
	}

	// Has the model normal?
	__declspec(dllexport) bool hasNormal()
	{
		return isNormal;
	}

	// Has the model UV?
	__declspec(dllexport) bool hasUV()
	{
		return isUV;
	}

	// Close file function can be empty because the file is closed at the end of read
	__declspec(dllexport) void closeFile()
	{
	}

	// Start a new triangle, reset the current vertex, normal and UV and step to the next triangle
	__declspec(dllexport) void getNewTriangle()
	{
		++currentTriangle;
		currentVertex = 0;
		currentNormal = 0;
		currentUV = 0;
	}

	// New vertex, normal and UV
	__declspec(dllexport) void getNewVertex()
	{
		++currentVertex;
		++currentNormal;
		++currentUV;
	}

	// Get the current vertex X coordinate
	__declspec(dllexport) float getNewVertexX()
	{
		unsigned int vertexIdx = triangles[currentTriangle - 1]->v[currentVertex - 1];
		return vertices[vertexIdx][0];
	}

	// Get the current vertex Y coordinate
	__declspec(dllexport) float getNewVertexY()
	{
		unsigned int vertexIdx = triangles[currentTriangle - 1]->v[currentVertex - 1];
		return vertices[vertexIdx][1];
	}

	// Get the current vertex Z coordinate
	__declspec(dllexport) float getNewVertexZ()
	{
		unsigned int vertexIdx = triangles[currentTriangle - 1]->v[currentVertex - 1];
		return vertices[vertexIdx][2];
	}

	// Get the current vertex W coordinate
	__declspec(dllexport) float getNewVertexW()
	{
		unsigned int vertexIdx = triangles[currentTriangle - 1]->v[currentVertex - 1];
		return vertices[vertexIdx][3];
	}

	// Get the current normal X coordinate
	__declspec(dllexport) float getNewNormalX()
	{
		unsigned int normalIdx = triangles[currentTriangle - 1]->n[currentNormal - 1];
		return normals[normalIdx][0];
	}

	// Get the current normal Y coordinate
	__declspec(dllexport) float getNewNormalY()
	{
		unsigned int normalIdx = triangles[currentTriangle - 1]->n[currentNormal - 1];
		return normals[normalIdx][1];
	}

	// Get the current normal Z coordinate
	__declspec(dllexport) float getNewNormalZ()
	{
		unsigned int normalIdx = triangles[currentTriangle - 1]->n[currentNormal - 1];
		return normals[normalIdx][2];
	}

	// Get the current UV U coordinate
	__declspec(dllexport) float getNewUVU()
	{
		unsigned int uvIdx = triangles[currentTriangle - 1]->t[currentUV - 1];
		return normals[uvIdx][0];
	}

	// Get the current UV V coordinate
	__declspec(dllexport) float getNewUVV()
	{
		unsigned int uvIdx = triangles[currentTriangle - 1]->t[currentUV - 1];
		return normals[uvIdx][1];
	}

	// Get the current UV W coordinate
	__declspec(dllexport) float getNewUVW()
	{
		unsigned int uvIdx = triangles[currentTriangle - 1]->t[currentUV - 1];
		return normals[uvIdx][2];
	}
}
